#include "Animal.h"
#include <iostream>

using namespace std;

Animal::Animal( int inStrength, int inCharisma )
	: strength( inStrength ), charisma( inCharisma )
{
}

Animal::~Animal()
{
	cout << "Animal destructor" << endl;
}

void Animal::talk() const
{
	cout << "What does the ";
}

void Animal::printStats() const
{
	cout << "Strength: " << strength << endl;
	cout << "Charisma: " << charisma << endl;
//	cout << "====================" << endl;
}