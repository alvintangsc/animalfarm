#include "Lion.h"
#include <iostream>

using namespace std;

Lion::Lion(int inStrength, int inCharisma)
	: Animal( inStrength, inCharisma ), maneFluffiness( 50 )
{
}



Lion::~Lion(void)
{
	cout << "Lion destructor" << endl;
}


void Lion::talk() const
{
	Animal::talk();
	cout << " Lion say?" << endl;
	cout << "Roar-or oar oar oar oar oh!" << endl;
}

void Lion::printStats() const
{
	cout << "-----Stats of Lion-----" << endl;
	Animal::printStats();
	cout << "Fluffiness of Mane: " << maneFluffiness << endl;
}