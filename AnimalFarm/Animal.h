#pragma once
class Animal
{
public:
	Animal( int inStrength = 0, int inCharisma = 0 );
	virtual ~Animal();
	virtual void talk() const;
	virtual void printStats() const;
private:
	int strength;
	int charisma;
};

