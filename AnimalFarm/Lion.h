#pragma once
#include "animal.h"

// Lion inherits from Animal class
class Lion : public Animal
{
public:

	//Lion();
	Lion( int inStrength = 0, int inCharisma = 0 );
	virtual ~Lion(void);
	virtual void talk() const; // overriding the talk function
	virtual void printStats() const;
private:
	int maneFluffiness;
};

