#pragma once
#include "animal.h"
class Fox : public Animal
{
public:
	Fox();
	virtual ~Fox();
	void talk() const;
	void talk( int inChatTimes ) const;

};

