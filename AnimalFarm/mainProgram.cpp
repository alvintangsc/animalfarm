#include "Lion.h"
#include "Fox.h"
#include "Cat.h"
#include <algorithm>
#include <deque>
using namespace std;
void main()
{
	deque<Animal*> theJungle;
	theJungle.push_back(new Lion());
	theJungle.push_back(new Fox());
	theJungle.push_back(new Cat());

	random_shuffle( theJungle.begin(), theJungle.end() );
	

	deque<Animal*>::iterator theItor = theJungle.begin();
	deque<Animal*>::iterator theEnd = theJungle.end();

	for( /*nothing*/; theItor != theEnd; theItor++ )
	{
		(*theItor)->talk();
	}

}