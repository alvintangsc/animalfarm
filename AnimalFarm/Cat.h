#pragma once
#include "animal.h"
class Cat :	public Animal
{
public:
	Cat(void);
	virtual ~Cat(void);
	virtual void talk() const;
};

